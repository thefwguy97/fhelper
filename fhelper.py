#!/usr/bin/env python
#
# GrovePi Project for a Plant monitoring project.
#	*	Reads the data from moisture, light, temperature and humidity sensor 
#		and takes pictures from the Pi camera periodically and logs them
#	*	Sensor Connections on the GrovePi:
#			-> Grove voltmeter sensor   	- Port A0
#			-> Grove Moisture sensor	- Port A1
#			-> Grove light sensor		- Port A2
#			-> Grove DHT sensors		- Port D4
#
#  The program create three separate data files :
#   - CSV sensor data file - contains the main data sensor collected
#   - simple sensor data file - same data but formatted for gnuplot
#   - datalogger data file - contains value of light and battery voltage
#   - program log
#
# NOTE:

'''
## License

The MIT License (MIT)

GrovePi for the Raspberry Pi: an open source platform for connecting Grove Sensors to the Raspberry Pi.
Copyright (C) 2015  Dexter Industries

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import time
import grovepi
import subprocess
import math
import os

#analog sensor port number
volt_sensor			= 0
mositure_sensor		= 1
light_sensor			= 2

#digital sensor
temp_humidity_sensor	= 4

#temp_humidity_sensor type
#	grove starter kit comes with the bluew sensor
blue			=0
white			=1

#############
# timings
time_for_sensor		= 60	# 60 seconds
time_for_datalogger	= 20	# 20 seconds
time_to_sleep		= 1	# 1 second

csv_file="fhelper_sensor.csv"
gnuplot_file="fhelper_sensor.txt"
datalogger_file="fhelper_datalogger.txt"
log_file="fhelper.log"

# Shutdown variables
shutdown_counter 	= 0
shutdown_count 		= 10
min_volt 		= 3250.00
min_light 		= 100
last_reading		= 0

# Sensors threshold
moisture_low		= 300
moisture_high		= 700

#Read the data from the environmental  sensors
# Returns moisture, temperature and humidity values or -1 if error
def read_sensor():
	try:
		[temp,humidity] = grovepi.dht(temp_humidity_sensor,white)
		moisture=grovepi.analogRead(mositure_sensor)
	except (IOError, TypeError):
		return [-1,-1,-1]

	if math.isnan(temp) or math.isnan(humidity):		#temp/humidity sensor sometimes gives nan
		return [-1,-1,-1]
	return [moisture,temp,humidity]

#Read for datalogger
# The voltmeter need to be set with the switch on 3, NOT 10
# The function returns the value of volt and the light sensor or -1 if error
def read_datalogger():
	try:
		readsum=grovepi.analogRead(volt_sensor)
		volt=3*readsum*4980/1023.00
		light=grovepi.analogRead(light_sensor)
	except (IOError, TypeError):
		return [-1,-1]

	return [volt, light]

# Shutdown utility
# The function analyze different parameters and decide to turn off (shutdown) the Raspberry Pi
# The function receive the last read value of light and volt
def decide_shutdown(volt, light):
	global shutdown_counter
	global shutdown_count
	global last_reading

	if volt < min_volt:
		# Battery below threeshold
		if volt >= last_reading:
			shutdown_counter = 0
			# Prepare entry Log file
			f=open(log_file,'a')
			f.write(" %s - Battery charging detected - current battery : %.2f\n" %(curr_time, voltreading/1000))
			f.close

			print("Battery charging")
		else:
			shutdown_counter += 1
			print("SHUTDOWN ZONE ! - %d " %(shutdown_count - shutdown_counter))

			# Prepare entry Log file
			f=open(log_file,'a')
			f.write(" %s - Shutdown detection  - current battery : %.2f\n" %(curr_time, voltreading/1000))
			f.close

			if shutdown_counter > shutdown_count:
				# Shutdown condition !
				print("SHUTDOWN ! \n")
			
				f=open(log_file,'a')
				f.write("%s\tSystem forced shutdown because %d\n" %(curr_time, voltreading/1000))
				f.close
				os.system("sudo shutdown -h now")  
	else:
		shutdown_counter = 0
	last_reading = volt
		
# ----------------- Main ----------------------------------

#Save the initial time, we will use this to find out when it is time to take a picture or save a reading
last_read_sensor=last_read_datalogger= int(time.time())
print("fHelper - flower Helper/n")
print("Datalogger time : %d sec - sensor time : %d sec" %(time_for_datalogger, time_for_sensor))
print("Shutdown battery level at : %.2f Volt" %(min_volt/1000))

voltreading = 0.0

# First read volt to assign last_reading variable
while voltreading == 0: 
	[voltreading, lightreading]=read_datalogger()
	if voltreading == -1:
		voltreading = 0

print("Current battery level at  : %.2f Volt\n\nProgram START !" %(voltreading/1000))

# Write in the files initial info
curr_time = time.strftime("%Y-%m-%d:%H-%M-%S")
# Datalogger 
f=open(datalogger_file,'a')
f.write("# %s - Starting datalogger - time - light - volt \n" %(curr_time))
f.close

# Prepare CSV file
f=open(csv_file,'a')
f.write("# %s - Starting data collection (CSV) - time - moisture - temperature - humidity\n" %(curr_time))
f.close()

# Prepare Gnu Plot file
f=open(gnuplot_file,'a')
f.write("# %s - Starting data collection (Gnuplot) - time - moisture - temperature - humidity\n" %(curr_time))
f.close

# Prepare Log file
f=open(log_file,'a')
f.write("----------------------------------------------------------------------------\n")
f.write(" %s - Starting log\n" %(curr_time))
f.write("Datalogger time : %d sec - sensor time : %d sec\n" %(time_for_datalogger, time_for_sensor))
f.write("Shutdown battery level at : %.2f Volt\n" %(min_volt/1000))
f.write("Current battery level at  : %.2f Volt\n\nProgram START !\n\n" %(voltreading/1000))
f.close

while True:
	curr_time_sec=int(time.time())

	# If it is time to handle the datalogger
	if curr_time_sec - last_read_datalogger > time_for_datalogger:
                [voltreading, light]=read_datalogger()
		curr_time = time.strftime("%Y-%m-%d:%H-%M-%S")

		if voltreading == -1:
			print("Bad datalogger reading")
		else:
			print(("Time  : %s\nLight : %d\nVolt  : %.2f \n" %(curr_time,light,voltreading/1000)))
			# Prepare datalogger log file
			f=open(datalogger_file,'a')
			f.write("%s\t%d\t%.2f\n" %(curr_time,light,voltreading/1000))
			f.close

			# SHutdown test
			decide_shutdown(voltreading, light)

		
		#Update the last read time
		last_read_datalogger=curr_time_sec

	
	# If it is time to take the sensor reading
	if curr_time_sec-last_read_sensor>time_for_sensor:
		[moisture,temp,humidity]=read_sensor()

               # If any reading is a bad reading, skip the loop and try again
		if moisture==-1:
			print("Bad reading")
			time.sleep(1)
			continue

		curr_time = time.strftime("%Y-%m-%d:%H-%M-%S")
		print(("Time : %s\nMoisture : %d\nTemp : %.2f\nHumidity : %.2f %%\n" %(curr_time,moisture,temp,humidity)))
		
		# Save the sensor reading to the CSV file
		f=open(csv_file,'a')
		f.write("%s,%d,,%.2f,%.2f;\n" %(curr_time,moisture,temp,humidity))
		f.close()

		# Prepare Gnu Plot file
		f=open(gnuplot_file,'a')
		f.write("%s\t%d\t%.2f\t%.2f\n" %(curr_time,moisture,temp,humidity))
		f.close

		#Update the last read time
		last_read_sensor=curr_time_sec
		
	#Slow down the loop
	time.sleep(time_to_sleep)
